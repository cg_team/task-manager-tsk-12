package ru.inshakov.tm.api.command;

import ru.inshakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
