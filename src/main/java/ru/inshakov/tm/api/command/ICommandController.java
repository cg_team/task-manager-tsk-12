package ru.inshakov.tm.api.command;

public interface ICommandController {

    void exit();

    void showAbout();

    void showHelp();

    void showVersion();

    void showCommands();

    void showArguments();

    void showSystemInfo();
}
