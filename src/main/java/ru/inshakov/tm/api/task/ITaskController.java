package ru.inshakov.tm.api.task;

import ru.inshakov.tm.model.Task;

public interface ITaskController {

    void showList();

    void create();

    void showTask(Task task);

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    void clear();

}
